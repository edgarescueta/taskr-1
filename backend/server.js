const config = require('./config');

const mongoose = require('mongoose');
const cors = require('cors');
const express = require('express');

const app = express();

const tasks = require('./routes/tasks');
app.use(cors());

mongoose.connect(config.connectionString, {useNewUrlParser: true }, () => {
	console.log('remote connection established!') 
});

app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.use('/api/tasks', tasks);

app.listen(config.port, () => console.log(`The server is listening to port: ${config.port}`
));