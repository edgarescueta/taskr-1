const express = require('express');
const mongoose = require('mongoose');
const router= express.Router();

const TaskModel = require('../models/Task');


router.get('/', async(req, res) => {
	let tasks = await TaskModel.find();
	res.send(tasks);
});

router.post('/', async(req, res) => {

	let task = new TaskModel({
		taskName: req.body.task
	});

	task = await task.save();

	res.send(task);

});


router.put('/:id', async (req, res) => {
	let task = await TaskModel.findByIdAndUpdate(req.params.id, {
			taskName: req.body.task
		}, {new: true});

	res.send(task);
});


router.delete('/:id', async (req, res) => {
	let task = await TaskModel.findByIdAndRemove(req.params.id)
	res.send(task);
});


router.post('/:taskId/subtasks', async(req, res) => {
	//find
	let task = await TaskModel.findById(req.params.taskId);

	//create object subtask
	let subTask = {
		taskName: req.body.task,
	}

	//push the object to subTask in Schema model
	task.subTasks.push(subTask);
	// save

	task = await task.save();

	res.send(task);

});


module.exports = router;



